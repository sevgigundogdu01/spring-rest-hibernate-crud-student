package service;

import dao.DepartmentDao;
import dto.AdressDto;
import dto.DepartmentDto;
import model.Adress;
import model.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentDao departmentDao;

    @Transactional
    public int save(Department department) {

        departmentDao.save(department);
        return department.getId();
    }

    @Transactional
    public void deleteDepartment(int id) {

        departmentDao.deleteDepartment(id);
    }


    @Transactional
    public void updateDepartment(int id, Department department) {

        departmentDao.updateDepartment(id,department);
    }

    public void updateDepartmentDTO(int id, DepartmentDto departmentDto) {

        Department department=new Department(departmentDto);
        departmentDao.updateDepartment(id,department);
    }


    public List getListDepartment() {

        return departmentDao.getListDepartment();
    }

    public Department get(int id) {

       return departmentDao.get(id);
    }

    public List<DepartmentDto> prepareList(){

        List<DepartmentDto> departmentDtoList=new ArrayList<DepartmentDto>();

        List<Department> departmentList=departmentDao.getListDepartment();

        for(Department department:departmentList){

            DepartmentDto departmentDto=new DepartmentDto(department,true, false);
            departmentDtoList.add(departmentDto);
        }

        return departmentDtoList;
    }
}
