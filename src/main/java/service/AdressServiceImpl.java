package service;

import dao.AdressDao;
import dto.AdressDto;
import model.Adress;
import model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class AdressServiceImpl implements AdressService {

    @Autowired
    private AdressDao adressDao;

    public List getListAdress() {
       return adressDao.getListAdress();
    }

    public Adress get(int id) {

       return adressDao.get(id);
    }

    @Transactional
    public int save(Adress adress) {

        adressDao.save(adress);
        return adress.getId();
    }

    @Transactional
    public void deleteAdress(int id) {

        adressDao.deleteAdress(id);

    }

    @Transactional
    public void updateAdress(int id, Adress adress) {

        adressDao.updateAdress(id,adress);
    }

    public void updateAdressDTO(int id, AdressDto adressDto) {

        Adress adress=new Adress(adressDto);
        adressDao.updateAdress(id,adress);
    }

    public List<AdressDto> prepareList() {

        List<AdressDto> returnList=new ArrayList<AdressDto>();

        List<Adress> adressList=adressDao.getListAdress();

        for (Adress adress:adressList){

            AdressDto adressDto=new AdressDto(adress);

            returnList.add(adressDto);

        }
        return returnList;
    }
}
