package service;

import dto.AdressDto;
import model.Adress;


import java.util.List;

public interface AdressService {

    List getListAdress();

    Adress get(int id);

    int save(Adress adress);

    void deleteAdress(int id);

    void updateAdress(int id, Adress adress);

    void updateAdressDTO(int id, AdressDto adressDto);

    List<AdressDto> prepareList();
}
