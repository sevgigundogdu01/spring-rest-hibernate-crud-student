package dao;

import model.Adress;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class AdressDaoImpl implements AdressDao {

    @Autowired
    private SessionFactory sessionFactory;

    public List getListAdress() {

        Session session=sessionFactory.getCurrentSession();
        CriteriaBuilder cb=session.getCriteriaBuilder();
        CriteriaQuery<Adress> cq=cb.createQuery(Adress.class);
        Root<Adress> root=cq.from(Adress.class);
        cq.select(root);

       Query<Adress> adressQuery= session.createQuery(cq);
       return adressQuery.getResultList();
    }

    public Adress get(int id) {

       return sessionFactory.getCurrentSession().get(Adress.class,id);
    }

    public int save(Adress adress) {

        sessionFactory.getCurrentSession().save(adress);
        return adress.getId();
    }

    public void deleteAdress(int id) {

        Session session=sessionFactory.getCurrentSession();
        Adress adress=session.byId(Adress.class).load(id);
        session.delete(adress);
    }

    public void updateAdress(int id, Adress adress) {

       Session session= sessionFactory.getCurrentSession();
       Adress adress1=session.byId(Adress.class).load(id);
       adress1.setCity(adress.getCity());
       adress1.setCountry(adress.getCountry());
       adress1.setStreet(adress.getStreet());
       session.flush();
    }

    public void updateAdressDto(int id, Adress adress) {

        Session session= sessionFactory.getCurrentSession();
        Adress adress1=session.byId(Adress.class).load(id);
        adress1.setCity(adress.getCity());
        adress1.setCountry(adress.getCountry());
        session.flush();
    }
}
