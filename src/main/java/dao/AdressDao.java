package dao;
import model.Adress;

import java.util.List;

public interface AdressDao {

    List getListAdress();

    Adress get(int id);

    int save(Adress adress);

    void deleteAdress(int id);

    void updateAdress(int id, Adress adress);
}
