package controller;

import dto.AdressDto;
import model.Adress;
import model.Department;
import model.Student;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.AdressService;
import service.DepartmentService;
import service.StudentService;

import java.util.List;

@RestController
public class AdressStudentController {


    @Autowired
    private AdressService adressService;

    @Autowired
    private StudentService studentService;

    // Using DTO
    @GetMapping("/adressList")
    public List<AdressDto> listAdressDto(){

       return adressService.prepareList();
    }

    @PostMapping("/saveAdress")
    public boolean saveAdress(@RequestBody AdressDto adressDto) {

        Adress adress = new Adress();
        adress.setCity(adressDto.getCity());
        adress.setCountry(adressDto.getCountry());

        adressService.save(adress);

        return true;

    }

    @PutMapping("/adressDTOUpdate/{id}")
    public ResponseEntity<?> updateAdress(@PathVariable("id") int id,@RequestBody AdressDto adressDto){

        adressService.updateAdressDTO(id,adressDto);
        return ResponseEntity.ok().body("Adress has been updated successfully");

    }


    // ** Using DTO

    @PostMapping("/adress")
    public ResponseEntity<?> save(@RequestBody Adress adress){

        int id= adressService.save(adress);
        return ResponseEntity.ok().body("New adress save with by :" +id);

    }

    @GetMapping("/adress")
    public ResponseEntity<List<Adress>> list(){

        List<Adress> adressList=adressService.getListAdress();
        return ResponseEntity.ok().body(adressList);

    }

    @GetMapping("/adress/{id}")
    public ResponseEntity<Adress> get(@PathVariable("id") int id){

        Adress adress=adressService.get(id);
        if(adress==null){

            ResponseEntity.ok().body(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(adress);
    }

    @PutMapping("/adressUpdate/{id}")
    public ResponseEntity<?> update(@PathVariable("id") int id,@RequestBody Adress adress){

        adressService.updateAdress(id,adress);
        return ResponseEntity.ok().body("Adress has been updated successfully");

    }

    @DeleteMapping("/adressDelete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") int id){

        adressService.deleteAdress(id);

        return ResponseEntity.ok().body("Adress has been deleted successfully");
    }

    @PostMapping("/adress/{id}/students")
    public ResponseEntity<?> save(@PathVariable int id,@RequestBody Student student) {

        Adress adress1=adressService.get(id);
        student.setAdress(adress1);
        int std_id=studentService.save(student);

        return ResponseEntity.ok().body("New Student has been saved with ID:" + std_id);
    }

}
