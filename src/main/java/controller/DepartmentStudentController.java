package controller;

import dto.AdressDto;
import dto.DepartmentDto;
import dto.StudentDto;
import model.Adress;
import model.Department;
import model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import service.AdressService;
import service.DepartmentService;
import service.StudentService;

import java.util.ArrayList;
import java.util.List;

@RestController
public class DepartmentStudentController {

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private AdressService adressService;


    // Using DTO

    @GetMapping("/departmentsList")
    public List<DepartmentDto> listDepartments(){

        return departmentService.prepareList();

    }

    @GetMapping("/studentsList")
    public List<StudentDto> listStudents(){

        return studentService.prepareList();

    }

    @RequestMapping(value = "/studentsSave",method = RequestMethod.POST)
    public boolean saveStudent(
            @RequestBody StudentDto dto
    ){
        Student student = new Student();

        student.setName(dto.getName());
        student.setSurname(dto.getSurname());

        Department department=new Department(dto.getDepartment());
        student.setDepartment(department);

        Adress adress=new Adress(dto.getAdress());
        student.setAdress(adress);

        departmentService.save(department);
        studentService.save(student);
        adressService.save(adress);

        return true;
    }

    @PostMapping("/departmentsSave")
    public boolean saveDepartment( @RequestBody DepartmentDto departmentDto){

        Department department=new Department();
        department.setId(departmentDto.getId());
        department.setDeptName(departmentDto.getName());


        for(StudentDto studentDto:departmentDto.getStudentDtoList()){

            Student student=new Student(studentDto);
            List<Student> studentList=new ArrayList<Student>();
            department.setStudent(studentList);

        }
        departmentService.save(department);
        return true;
    }

    @PutMapping("/departmentDTOUpdate/{id}")
    public ResponseEntity<?> updateDepartmentDto(@PathVariable int id,@RequestBody DepartmentDto departmentDto){

        departmentService.updateDepartmentDTO(id,departmentDto);
        return ResponseEntity.ok().body("Department has been updated successfully");
    }

    @PutMapping("studentDTOUpdate/{id}")
   public ResponseEntity<?> updateStudentDto(@PathVariable int id,@RequestBody StudentDto studentDto){

        studentService.updateStudent(id,studentDto);
        return ResponseEntity.ok().body("Student has been updated successfully");
   }

    // ** Using DTO


    // related Departments
    // save Department
    @PostMapping("/departments")
    public ResponseEntity<?> save(@RequestBody Department department){

       int id= departmentService.save(department);
        return ResponseEntity.ok().body("New department save with by :" +id);

    }

    @PostMapping("/departments/{id}/students")
    public ResponseEntity<?> save(@PathVariable int id,@RequestBody Student student) {

        Department department1=departmentService.get(id);
        student.setDepartment(department1);
        int std_id=studentService.save(student);

        return ResponseEntity.ok().body("New Student has been saved with ID:" + std_id);
    }

    @GetMapping("/departments")
    public ResponseEntity<List<Department>> list(){

        List<Department> departmentList=departmentService.getListDepartment();
        return ResponseEntity.ok().body(departmentList);

    }

    @GetMapping("/departments/{id}")
    public ResponseEntity<Department> get(@PathVariable("id") int id){

        Department department=departmentService.get(id);
        if(department==null){

            ResponseEntity.ok().body(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(department);
    }

    @PutMapping("/departments/{id}")
    public ResponseEntity<?> update(@PathVariable("id") int id,@RequestBody Department department){

        departmentService.updateDepartment(id,department);
        return ResponseEntity.ok().body("Department has been updated successfully");

    }

    @DeleteMapping("/departmentsDelete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") int id){

        departmentService.deleteDepartment(id);

        return ResponseEntity.ok().body("Department has been deleted successfully");
    }

    // ** related departments

    @GetMapping("/getStudents/{id}")
    public ResponseEntity<Student> getStudents(@PathVariable("id") int id){

       Student student= studentService.get(id);

        return ResponseEntity.ok().body(student);
    }

    @PutMapping("/studentUpdate/{id}")
    public ResponseEntity<?> updateStudent(@PathVariable("id") int id,@RequestBody Student student){

        studentService.updateStudent(id,student);
        return ResponseEntity.ok().body("Student has been updated successfully "+id);

    }

    @DeleteMapping("/studentDelete/{id}")
    public ResponseEntity<?> deleteStudent(@PathVariable("id") int id){

        studentService.deleteStudent(id);

        return ResponseEntity.ok().body("Student has been deleted successfully"+id);
    }
}
