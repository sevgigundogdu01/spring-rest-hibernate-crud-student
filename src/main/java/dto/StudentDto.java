package dto;

import model.Student;

public class StudentDto {

    public StudentDto() {
    }

    public StudentDto(Student student,boolean isDepartmentNeeded) {
        this.setName(student.getName());
        this.setSurname(student.getSurname());
        if(isDepartmentNeeded && student.getDepartment() != null) {
            DepartmentDto dept = new DepartmentDto(student.getDepartment(),false, false);
            this.setDepartment(dept);
        }
        if(student.getAdress() != null){
            AdressDto adressDto=new AdressDto(student.getAdress());
            this.setAdress(adressDto);
        }
    }

    private String name;
    private String surname;
    private DepartmentDto department;
    private AdressDto adress;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public DepartmentDto getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentDto department) {
        this.department = department;
    }

    public AdressDto getAdress() {
        return adress;
    }

    public void setAdress(AdressDto adress) {
        this.adress = adress;
    }
}
