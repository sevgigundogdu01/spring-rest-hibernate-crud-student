package dto;

import model.Department;
import model.Student;

import java.util.ArrayList;
import java.util.List;

public class DepartmentDto {
    public DepartmentDto(){

    }

    public DepartmentDto(Department department, boolean isStudentNeeded, boolean isTest) {
        this.setId(department.getId());
        this.setName(department.getDeptName());

        List<Student> studentList=department.getStudent();
        List<StudentDto> studentDtoList=new ArrayList<StudentDto>();

        if(isStudentNeeded && department.getStudent()!=null){

            for(Student student:studentList){

                StudentDto studentDto=new StudentDto(student,false);
                studentDtoList.add(studentDto);

            }
            this.setStudentDtoList(studentDtoList);
        }
    }

    private int id;
    private String name;
    private List<StudentDto> studentDtoList;

    public List<StudentDto> getStudentDtoList() {
        return studentDtoList;
    }

    public void setStudentDtoList(List<StudentDto> studentDtoList) {
        this.studentDtoList = studentDtoList;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



}
