package model;

import dto.AdressDto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Adress implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "adress_id")
    private int id;
    @Column(name = "adress_city")
    private String city;
    @Column(name = "adress_country")
    private String country;
    @Column(name="adress_street")
    private String street;

/*
    @OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL,mappedBy = "adress")
    private Student student;*/

    public Adress() {
    }

    public Adress(AdressDto adressDto){

        this.setCity(adressDto.getCity());
        this.setCountry(adressDto.getCountry());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
}
