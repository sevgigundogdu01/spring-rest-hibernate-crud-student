package model;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import dto.AdressDto;
import dto.DepartmentDto;
import dto.StudentDto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
/*@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id"
)*/
public class Student implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @JsonProperty("id")
    private int id;
    private String name;
    private String surname;
    private String schoolNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="department_id")
//    @JsonBackReference
    @JsonIgnore
//    @JsonDeserialize
    private Department department;

    @OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name = "adress_id",unique = true)
    private Adress adress;


    public Student() {
    }

    public Student(StudentDto studentDto) {
        this.setName(studentDto.getName());
        this.setSurname(studentDto.getSurname());
        if(studentDto.getDepartment() != null) {
           Department department=new Department(studentDto.getDepartment());
            this.setDepartment(department);
        }

        if(studentDto.getAdress() != null){

            Adress adress=new Adress(studentDto.getAdress());
            this.setAdress(adress);

        }
    }





    public Adress getAdress() {
        return adress;
    }

    public void setAdress(Adress adress) {
        this.adress = adress;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", schoolNumber='" + schoolNumber + '\'' +
                ", department=" + department +
                '}';
    }
}
